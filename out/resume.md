Raul Lezameta
============
Email: lezametaraul@gmail.com
Tel: +39 373 8747945
Web: https://gitlab.com/raul.lezameta/cv

Masters student of [Geodesy and Geoinformation](https://www.tuwien.at/en/studies/studies/master-programmes/geodesy-and-geoinformation/) at **TU Vienna**

## SKILLS

  - Data science: Pyhton Matlab HTML 5 
  - GIS software: QGIS ArcMap 
  - Web development: HTML CSS JavaScript 

## EMPLOYMENT

### *Internhsip*, [Südtiroler Informatik AG](http://www.siag.it/) (2017-07 — 2017-08)

GIS tasks

### *Tutor*, [TU Vienna](https://www.tuwien.at/en/) (2009-07 — 2013-10)

Tutoring of the courses **Web Mapping** and **Multimedia Cartography**
  - Map creation with web technologies
  - Adding **animation** and **interactivity** to web maps (video maps, ...)




## EDUCATION

### [TU Vienna](https://www.tuwien.at/en/) (2015-10 — 2018-10)



### [TU Vienna](https://www.tuwien.at/en/) (2018-10 — Present)













## INTERESTS

- SPORTS

- WEB, TECHNOLOGY AND IT


